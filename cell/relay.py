from  cell.cell import Cell
import netstruct

RELAY_CMD = 3
field_names = ['circID', 'cmd', 'rcmd', 'recognized', 'streamID', 'digest', 'length', 'payload']
RCMDS = {'EXTEND': 1, 'DATA': 2, 'BEGIN': 3, 'CONNECTED': 4, 'EXTENDED': 5}
 
class Relay(Cell):

    def __init__(self, circ_id, rcmd, streamID, recognized, digest, length, data) -> None:
        super().__init__(circ_id, RELAY_CMD)
        self._rcmd = rcmd
        self._streamID = streamID
        self._recognized = recognized
        self._digest = digest
        self._length = length
        self._data = data
        self._msg = b''

    def create_msg(self) -> bytearray:
        self._msg = netstruct.pack(b"i B B 2s h 4s h", self._circ_id, self._cmd, self._rcmd,self._recognized,
            self._streamID, self._digest,self._length) + self._data
        return self._msg

    @staticmethod
    def unpack_msg(msg):
        unpacked = netstruct.unpack(b"i B B 2s h 4s h", msg)
        dict = {k: v for k, v in zip(field_names, unpacked)}
        dict['payload'] = msg[-dict['length']:]
        return dict

    def set_data(self, data):
        self._data = data
        self._length = len(data)

    def get_data(self):
        return self._data

    def printrelay(self):
        print (self._circ_id, self._cmd, self._rcmd,self._recognized,
            self._streamID, self._digest,self._length, self._data)

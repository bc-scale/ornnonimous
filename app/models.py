from email.policy import default
from . import db
from flask_login import UserMixin
from datetime import datetime 

class User(UserMixin, db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(256))
    name = db.Column(db.String(100))


class Chat(db.Model):
    __tablename__ = 'chat'
    id = db.Column(db.Integer, primary_key=True) 
    A_userId = db.Column(db.Integer())
    B_userId = db.Column(db.Integer())
    
class Message(db.Model):
    __tablename__ = 'message'
    id = db.Column(db.Integer, primary_key=True) 
    chatId = db.Column(db.Integer(), db.ForeignKey('chat.id'))
    sender = db.Column(db.Integer())
    date = db.Column(db.DateTime, default=datetime.utcnow)
    msg = db.Column(db.String(200), nullable=False)

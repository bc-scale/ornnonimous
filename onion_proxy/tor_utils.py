import socket
import sys
import os
from Cryptodome.PublicKey import RSA as RSACipher
current_dir = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current_dir)

sys.path.append(parent)
from cell.create import Create, HTYPES
from cell.created import Created, CREATED_CMD
from cell.relay import Relay, RCMDS
from cell.destroy import Destroy, DESTROY_CODES
from AES import Aes
import RSA
import encapsulate
from decapsulate import decapsulate, decapsulate_all

BUFFER_SIZE = 100000

def create_circuit(relay_list):
    '''
    Creating a full tor circuit
    input: list of dictionaries (nodes), each dictionary containing the node address, AES key, and a circuit-ID.
    output: socket to entry
    '''
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(relay_list[0]['connection'])
    for i in range(len(relay_list)):
        pub_key = extend_circuit(s, relay_list[0:i], relay_list[i])
        establish_symetric_key(s, relay_list[0:i], relay_list[i], pub_key)
    return s

def extend_circuit(connection, prev_relay_list, new_relay):
    '''
    Extending number of nodes in the circuit
    input: conection if there is one, previous relays + keys, new relay
    output: new relay's RSA public key / -1 if unsuccessful 
    '''
    public_key_reqest =  Create(new_relay['circ_id'], HTYPES['GET_PUB'], 0, b'')
    packed_msg = b''

    if len(prev_relay_list) == 0: 
        # if this is the entry node
        packed_msg = public_key_reqest.create_msg()
    else:
        # creating relay cells for all the current nodes, except the last one
        relay_cells_list = []
        if len(prev_relay_list) > 1: # if == 1 there is no need in relay data
            temp2 = prev_relay_list[0:-1]
            for node_info in temp2:
                relay_cells_list.insert(0,[node_info['Aes_key'], Relay(circ_id=node_info['circ_id'], rcmd=RCMDS['DATA'], streamID=1, recognized=b'1', digest=b'1', length=0, data=b'')])

        # create extand relay msg 
        new_node_ip = bytes(map(int, new_relay['connection'][0].split('.')))
        new_node_port = new_relay['connection'][1].to_bytes(2, byteorder='big')
        extend_data = new_node_ip + new_node_port + public_key_reqest.create_msg()

        # adding extend cell to list
        node_to_send_extend = prev_relay_list[-1]
        relay_cells_list.insert(0,[node_to_send_extend['Aes_key'], Relay(circ_id=node_to_send_extend['circ_id'], rcmd=RCMDS['EXTEND'], streamID=1, recognized=b'0', digest=b'1', length=len(extend_data), data=extend_data)])
        
        # encapsulating tor packet
        packed_msg = encapsulate.encapsulate(relay_cells_list)
    
    connection.sendall(packed_msg)  # sending encapsulated cell
    result = connection.recv(2048)
    temp = [aes_key['Aes_key'] for aes_key in prev_relay_list]
    result = decapsulate_all(result, temp)
    result = Created.unpack_msg(result)
    return result['hData']


def establish_symetric_key(connection, prev_relay_list, new_relay, pub):
    '''
    Establish AES key with a node
    input: previous relays + keys, new relay, public key
    output: true/false
    '''
    pub = RSACipher.import_key(pub)
    enc_key = RSA.encrypt(new_relay['Aes_key'].get_key(), pub)

    # RELAY_DATA will contain the create request (last relay's payload)
    request =  Create(new_relay['circ_id'], HTYPES['SHARED_SECRET'], len(enc_key), enc_key).create_msg()
    encapsulated = request

    if len(prev_relay_list) > 0:
        # Creating a Relay object for the other relays left and insetring to list
        key_cell_list = []
        for relay in prev_relay_list[:-1]:
            new_relay = Relay(circ_id=relay['circ_id'], rcmd=RCMDS['DATA'], streamID=1, recognized=b'1', digest=b'1', length=0, data=b'')
            key_cell_list += [[relay['Aes_key'], new_relay]]
        
        request_relay = Relay(circ_id=prev_relay_list[-1]['circ_id'], rcmd=RCMDS['DATA'], streamID=1, recognized=b'0', digest=b'1', length=len(request), data=request)
        key_cell_list += [[prev_relay_list[-1]['Aes_key'], request_relay]]

        encapsulated = encapsulate.encapsulate(key_cell_list[::-1])

    connection.sendall(encapsulated)
    res = connection.recv(2048)

    keys = [relay['Aes_key'] for relay in prev_relay_list]
    decapsulated = decapsulate_all(res, keys)
    return decapsulated



def connect_to_server(connection, relay_list, server_address):
    """
    Connect the exit node to the sever
    input: connection to entry, relay list, server address
    output: exit node response
    """
    key_cell_list = []

    # Making the relay Begin request to the exit relay
    new_server_ip = bytes(map(int, server_address[0].split('.')))
    new_server_port = server_address[1].to_bytes(2, byteorder='big')
    data = new_server_ip + new_server_port
    request = Relay(circ_id=relay_list[-1]['circ_id'], rcmd=RCMDS['BEGIN'], streamID=1, recognized=b'0', digest=b'1', length=len(data), data=data)
    
    # Encapsulating a full tor packet
    for relay in relay_list[0:-1]:
        key_cell_list.insert(0,[relay['Aes_key'], Relay(circ_id=relay['circ_id'], rcmd=RCMDS['DATA'], streamID=1, recognized=b'1', digest=b'1', length=0, data=b'')])
    key_cell_list.insert(0,[relay_list[-1]['Aes_key'], request])
    encapsulated = encapsulate.encapsulate(key_cell_list)

    connection.sendall(encapsulated)
    res = connection.recv(2048)
    keys = [relay['Aes_key'] for relay in relay_list]
    decapsulated = decapsulate_all(res, keys)
    return decapsulated

def send_data_to_server(connection, relay_list, data_to_send):
    """
    Sending and receiving data from server
    input: connection to entry, relay list, data for server
    output: server response
    """
    key_cell_list = []
    request = Relay(circ_id=relay_list[-1]['circ_id'], rcmd=RCMDS['DATA'], streamID=1, recognized=b'\x00\x00', digest=b'1', length=len(data_to_send), data=data_to_send)

    for relay in relay_list[0:-1]:
        key_cell_list.insert(0,[relay['Aes_key'], Relay(circ_id=relay['circ_id'], rcmd=RCMDS['DATA'], streamID=1, recognized=b'1', digest=b'1', length=0, data=b'')])
    
    key_cell_list.insert(0,[relay_list[-1]['Aes_key'], request])
    encapsulated = encapsulate.encapsulate(key_cell_list)
    connection.sendall(encapsulated)
    
    res = connection.recv(BUFFER_SIZE)
    print('recieved from entry: ', res)
    print('response length: ', len(res))
    keys = [relay['Aes_key'] for relay in relay_list]
    decapsulated = decapsulate_all(res, keys)
    return decapsulated


def destroy_circuit(s, entry_node):
    # Sending a Destroy packet to tear the circuit down
    req = Destroy(entry_node['circ_id'], DESTROY_CODES['DESTROYED']).create_msg()
    s.sendall(req)

from  cell.cell import Cell
import netstruct
import itertools

CREATED_CMD = 2
field_names = ['circID', 'cmd', 'hLen', 'hData']

class Created(Cell):
    def __init__(self, circId, hLen, hData):
        super().__init__(circId, CREATED_CMD)
        self._hLen = hLen
        self._hData = hData

    def create_msg(self):
        return netstruct.pack(b"i B h", self._circ_id, self._cmd, self._hLen) + self._hData
    
    @staticmethod
    def unpack_msg(msg):
        unpacked = netstruct.unpack(b"i B h", msg)
        dict = {k: v for k, v in zip(field_names, unpacked)}
        dict['hData'] = msg[7:]
        return dict



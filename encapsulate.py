from  cell.create import Create
from  cell.created import Created
from  cell.relay import Relay
from AES import Aes


"""
get -> cell_key_list = [[Aes,Cell],[Aes,Cell]......]
"""
def encapsulate(cell_key_list):
    """
    Encapsulate list of tor cells and their symetric key
    Params: cell_key_list - list of tor cells and their symetric key
    Return: encapsulated cell
    """
    while len(cell_key_list):
        in_aes, in_msg = cell_key_list[0]
        inerMsg = encrypt_msg(in_aes,in_msg.create_msg())
        if len(cell_key_list)==1: # got to end of cells 
            return inerMsg
        else: 
            cell_key_list[1][1].set_data(inerMsg)
        cell_key_list.remove([in_aes, in_msg])
    return inerMsg


def encrypt_msg(aes, msg):
    headers = msg[:5]
    payload = aes.encrypt(msg[5:])
    return headers+payload

from  cell.cell import Cell
import netstruct
import itertools

DESTROY_CMD = 4
DESTROY_CODES = {'CHANNEL_CLOSED': 1,
                 'CONNECT_FAILED': 2, 
                 'DESTROYED': 3}
field_names = ['circID', 'cmd', 'code']

class Destroy(Cell):
    def __init__(self, circId,code):
        super().__init__(circId, DESTROY_CMD)
        self._code = code


    def create_msg(self):
        return netstruct.pack(b"i B B", self._circ_id, self._cmd, self._code)
    
    @staticmethod
    def unpack_msg(msg):
        unpacked = netstruct.unpack(b"i B B", msg)
        dict = {k: v for k, v in zip(field_names, unpacked)}
        return dict
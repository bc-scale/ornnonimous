from abc import ABC, abstractmethod

class Cell:
    def __init__(self, circ_id, cmd) -> None:
        self._circ_id = circ_id
        self._cmd = cmd

    @abstractmethod
    def create_msg(self) -> bytearray:
        pass


